import { useQuery } from "react-query";

import { ethInstance } from "evm-chain-scripts";

import { getTokenBalance } from "./bridge";

export const useTokenBalance = () =>
  useQuery("getTokenBalance", async () => getTokenBalance());
export const useChainId = () =>
  useQuery("getChainId", async () => ethInstance.getChainId());
export const useEthAccount = () =>
  useQuery("ethAccount", async () => ethInstance.getEthAccount());

export const useDestinationTokenBalance = (id) =>
  useQuery(["destinationBalance", id], async () => getTokenBalance(id), {
    enabled: !!id,
  });
