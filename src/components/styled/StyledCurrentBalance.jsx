import styled from "styled-components";

const StyledCurrentBalance = styled.span`
  font-family: "Work Sans";
  font-size: 16px;
  font-weight: 300;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: normal;
  text-align: left;
  color: #efefef;
  margin-top: 11px;
  margin-bottom: 46px;
`;

export default StyledCurrentBalance;
