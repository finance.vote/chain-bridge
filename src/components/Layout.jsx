import makeBlockie from "ethereum-blockies-base64";
import { ConnectWallet, ethInstance } from "evm-chain-scripts";
import arrowIcon from "images/icon-arrow.png";
import linkIcon from "images/link.png";
import { useEffect, useState } from "react";
import styled from "styled-components";
import chainsSelect from "../fixtures/chains";
import Footer from "./Footer";
import Logo from "./Logo";
import UnlockWallet from "./UnlockWallet";

const Dropdown = styled.div`
  position: absolute;
  top: 58px;
  right: 0;
  display: flex;
  flex-direction: column;
  width: 225px;
  flex-grow: 0;
  box-shadow: 0 4px 4px 0 #103147;
  border: solid 1.4px #00cfff;
  background-color: #103147;
  padding: 22px 0 18px 30px;
  box-sizing: border-box;
`;
const DropdownItem = styled.a`
  display: flex;
  text-decoration: none;
  align-items: center;
  width: 66px;
  height: 38px;
  flex-grow: 0;
  font-family: "Work Sans";
  font-size: 18px;
  font-weight: 300;
  line-height: 2.11;
  text-align: left;
  color: #efefef;
  white-space: nowrap;
  margin-bottom: 20px;
  & > img {
    margin-right: 23px;
    height: 20px;
  }
`;

const DropdownFooter = styled.div`
  width: 100%;
  margin-top: 19px;
  font-family: "Work Sans";
  font-size: 14px;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: 30px;
  letter-spacing: normal;
  text-align: left;
  color: rgba(239, 239, 239, 0.5);
  img {
    opacity: 50%;
    margin-right: 10px;
    position: relative;
    top: 3px;
  }
`;

const PageWrapper = styled.div`
  width: 1140px;
  margin: 0 auto;
  padding: 34px 0;

  @media (max-width: 1140px) {
    width: 960px;
  }

  @media (max-width: 768px) {
    width: 100%;
    padding: 15px;
    box-sizing: border-box;
  }
`;

const Header = styled.div`
  display: flex;
  justify-content: space-between;
  color: #ffffff;
`;
const WalletWrapper = styled.div`
  cursor: pointer;
  position: relative;
  display: flex;
  width: 250px;
  height: 50px;
  flex-grow: 0;
  background-color: #184366;
  align-items: center;
  justify-content: center;
  font-family: "Work Sans";
  font-size: 18px;
  font-weight: 300;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.75;
  letter-spacing: normal;
  text-align: center;
  color: #efefef;
  .wallet-icon {
    width: 25px;
    height: 25px;
    border-radius: 100%;
    flex-grow: 0;
    margin: 4px 10px 3px 0;
  }
  & > div:first-of-type {
    display: flex;
    align-items: center;
    justify-content: center;
    text-transform: uppercase;
    font-family: "Work Sans";
    font-weight: 300;
  }
`;

const Arrow = styled.span`
  margin-left: 21px;
  content: url(${arrowIcon});
  flex-grow: 0;
`;
const shortenWalletAddress = (address) => {
  return address ? `${address.substr(0, 6)}...${address.substr(-4)}` : "";
};
const mapChainToName = (chainId) => {
  const chain = chainsSelect.find((e) => e.chainId === parseInt(chainId));
  return chain ? (
    <>
      <img src={chain.icon} alt='icon' />
      {chain.label}
    </>
  ) : (
    <>Unsupported chain: {chainId}</>
  );
};
const Layout = ({ children }) => {
  const [address, setAddress] = useState();
  const [currentNetwork, setCurrentNetwork] = useState();
  const [showDropdown, setShowDropdown] = useState(false);
  const [avatar, setAvatar] = useState();
  const [showUnlockWallet, setShowUnlockWallet] = useState(false);

  useEffect(() => {
    window.ethereum?.on(
      "accountsChanged",
      ethInstance.handleAccountsChanged.bind(ethInstance)
    );
  }, []);

  async function update() {
    setShowUnlockWallet(true);
    const address = await ethInstance.getEthAccount();
    setAddress(address);
    setCurrentNetwork(await ethInstance.getChainId());
    setShowUnlockWallet(false);
  }
  useEffect(() => {
    address && setAvatar(makeBlockie(address));
  }, [address]);

  return (
    <>
      <PageWrapper>
        <Header>
          <Logo />
          {address && currentNetwork ? (
            <WalletWrapper>
              <div onClick={() => setShowDropdown(!showDropdown)}>
                <img src={avatar} alt='account_icon' className='wallet-icon' />
                {shortenWalletAddress(address)}
                <Arrow />
              </div>
              {showDropdown ? (
                <Dropdown>
                  <DropdownItem
                    href={`https://etherscan.io/address/${address}`}
                  >
                    <img src={linkIcon} alt='etherscan' />
                    Etherscan
                  </DropdownItem>
                  <DropdownFooter>
                    You're on
                    <br />
                    {mapChainToName(currentNetwork)}
                  </DropdownFooter>
                </Dropdown>
              ) : null}
            </WalletWrapper>
          ) : (
            <ConnectWallet onConnect={update} className='connect-wallet' />
          )}
          {showUnlockWallet ? <UnlockWallet /> : null}
        </Header>
        <main>{children}</main>
      </PageWrapper>
      <Footer />
    </>
  );
};

export default Layout;
