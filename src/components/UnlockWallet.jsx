import Popup from "./styled/Popup";
import styled from "styled-components";
import logo from "images/fvt-bridge.svg";
import logoLoader from "images/Bridge_loader.gif";

const Unlock = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  width: 214px;
  height: 241px;
  & > h3 {
    margin: 45px 0;
    width: 183px;
    height: 19px;
    flex-grow: 0;
    font-family: "Work Sans";
    font-size: 16px;
    font-weight: 500;
    text-align: center;
    color: #fff;
  }
  .logo {
    width: 214px;
    height: 30px;
  }
  .loader {
    width: 100px;
    height: 100px;
  }
`;

const UnlockWallet = () => {
  return (
    <Popup>
      <Unlock>
        <img src={logo} alt='logo' className='logo' />
        <h3>Unlock your wallet</h3>
        <img src={logoLoader} alt='loader' className='loader' />
      </Unlock>
    </Popup>
  );
};

export default UnlockWallet;
