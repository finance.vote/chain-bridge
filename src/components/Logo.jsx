import styled from "styled-components";
import logoIcon from "images/fvt-bridge.svg";

const LogoContainer = styled.div`
  img {
    max-width: 100%;
    width: 214px;
    height: 30px;
  }
`;

const Logo = () => {
  return (
    <LogoContainer>
      <img src={logoIcon} alt='finance.vote bridge' />
    </LogoContainer>
  );
};

export default Logo;
