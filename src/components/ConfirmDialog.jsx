import Popup from "./styled/Popup";
import styled from "styled-components";

const ConfirmButton = styled.button`
  width: 148px;
  height: 50px;
  flex-grow: 0;
  margin: 0 45px 0 0;
  background-color: #00b1ff;
  border: solid 2px #00b1ff;
  font-family: "Work Sans";
  font-size: 18px;
  font-weight: 600;
  letter-spacing: 3px;
  text-align: center;
  color: #efefef;
`;

const CancelButton = styled.button`
  width: 148px;
  height: 50px;
  flex-grow: 0;
  border: solid 2px #00b1ff;
  background: transparent;
  font-family: "Work Sans";
  font-size: 18px;
  font-weight: 600;
  letter-spacing: 3px;
  text-align: center;
  color: #efefef;
`;
const Text = styled.h2`
  width: 411px;
  flex-grow: 0;
  margin: 0 0 54.5px;
  font-family: "Work Sans";
  font-size: 20px;
  font-weight: 600;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.75;
  letter-spacing: 1px;
  text-align: center;
  color: #efefef;
`;

const ConfirmDialog = ({ amount, from, to, dialogSetter, transfer }) => {
  return (
    <Popup style={{ flexDirection: "column" }}>
      <Text>
        You’re moving {amount} $FVT from
        <br /> {from} to {to.label}?
      </Text>
      <div>
        <ConfirmButton onClick={transfer}>OKAY</ConfirmButton>
        <CancelButton onClick={dialogSetter}>CANCEL</CancelButton>
      </div>
    </Popup>
  );
};

export default ConfirmDialog;
