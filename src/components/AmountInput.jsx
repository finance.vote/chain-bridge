import errorIcon from "images/error.png";
import { useRef } from "react";
import styled from "styled-components";
import StyledCurrentBalance from "./styled/StyledCurrentBalance";

const StyledErrorBalance = styled.div`
  height: 16px;
  margin-top: 8px;
  flex-grow: 0;
  font-family: "Work Sans";
  font-size: 14px;
  font-weight: 300;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: normal;
  text-align: left;
  color: #ff6960;
  img {
    margin-left: 13px;
    position: relative;
    top: 1px;
  }
`;
const StyledLinkButton = styled.button`
  position: absolute;
  right: 15px;
  top: 50%;
  transform: translateY(-50%);
  background: none;
  border: none;
  font-family: "Work Sans";
  font-size: 15px;
  font-weight: 600;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: normal;
  color: #1ec0f3;
  cursor: pointer;
`;
const StyledInput = styled.input`
  padding: 31.3px 67px 31.3px 30px;
  background-color: #0e273b;
  border: none;
  font-family: "Work Sans";
  font-size: 32px;
  color: rgba(239, 239, 239, 1);
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: normal;
  text-align: left;
  box-sizing: border-box;
  max-width: 100%;
  &::placeholder {
    color: rgba(239, 239, 239, 0.5);
  }
  &:-ms-input-placeholder {
    color: rgba(239, 239, 239, 0.5);
  }
  &::-ms-input-placeholder {
    color: rgba(239, 239, 239, 0.5);
  }
  -moz-appearance: textfield;
  &::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
  }
`;

const AmountInput = ({ setTransferAmount, transferAmount, tokenBalance }) => {
  const amountInputRef = useRef();
  const handleChange = (event) => {
    setTransferAmount(event.target.value);
  };
  const setMaxTransferAmount = () => {
    const amount = tokenBalance;
    setTransferAmount(amount);
    amountInputRef.current.value = amount;
  };
  return (
    <>
      <div style={{ position: "relative" }}>
        <StyledInput
          placeholder={"0.0"}
          onChange={handleChange}
          type='number'
          ref={amountInputRef}
        />
        <StyledLinkButton type='button' onClick={setMaxTransferAmount}>
          MAX
        </StyledLinkButton>
      </div>
      <StyledCurrentBalance>
        Current $FVT Balance: {tokenBalance || "--"}
        {transferAmount > parseFloat(tokenBalance) ? (
          <StyledErrorBalance>
            Insufficient balance <img src={errorIcon} alt='error' />
          </StyledErrorBalance>
        ) : null}
      </StyledCurrentBalance>
    </>
  );
};

export default AmountInput;
