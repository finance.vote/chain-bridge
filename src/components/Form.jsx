import chainsSelect from "fixtures/chains";
import { useState } from "react";
import Select from "react-select";
import styled from "styled-components";
import { depositToBridge } from "utils/bridge";
import {
  useChainId,
  useDestinationTokenBalance,
  useTokenBalance,
} from "utils/queries";
import AmountInput from "./AmountInput";
import ConfirmDialog from "./ConfirmDialog";
import CustomPlaceholder from "./CustomPlaceholder";
import CustomSelectOption from "./CustomSelectOption";
import Button from "./styled/Button";
import StyledCurrentBalance from "./styled/StyledCurrentBalance";

const StyledForm = styled.form`
  display: flex;
  flex-direction: column;
`;

const StyledLabel = styled.label`
  flex-grow: 0;
  font-family: "Work Sans";
  font-size: 18px;
  font-weight: 600;
  font-stretch: normal;
  font-style: normal;
  line-height: 1;
  letter-spacing: normal;
  text-align: left;
  color: #efefef;
`;

const FormGroup = styled.div`
  display: flex;
  flex-direction: column;
  .tonetwork {
    flex-grow: 0;
    margin: 15.7px 0 0;
    border: solid 1px #acb2ab;
    background-color: #0e273b;
    font-family: "Work Sans";
    font-size: 16px;
    font-weight: normal;
    font-stretch: normal;
    font-style: normal;
    line-height: normal;
    letter-spacing: normal;
    text-align: left;
    color: #efefef;
    &__control {
      background: unset;
      border: 0;
      .tonetwork__value-container {
        padding: 0;
      }
    }
    &__menu {
      border-radius: 0;
      margin: 0;
      background: #0e273b;
    }
    &__placeholder {
      margin-left: 16px;
      color: #efefef;
    }
    &__indicator-separator {
      display: none;
    }
  }
`;

const StyledFromNetwork = styled.span`
  flex-grow: 0;
  margin: 15.7px 0 32.4px;
  border: solid 1px #acb2ab;
  background-color: #0e273b;
  font-family: "Work Sans";
  font-size: 16px;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: normal;
  text-align: left;
  color: #efefef;
`;

const StyledUnsupported = styled.div`
  padding: 9.8px 18.9px 9.7px 13px;
`;

const filterAvailable = (list, currentNetwork) => {
  return list.filter((item) => {
    return item.chainId !== currentNetwork;
  });
};

const destinationToName = (destination) => {
  return chainsSelect.find((x) => x.value === destination);
};

const Form = () => {
  const { data: chainId } = useChainId();
  const { data: tokenBalance } = useTokenBalance();
  const [transferAmount, setTransferAmount] = useState(0);
  const [showConfirmDialog, setShowConfirmDialog] = useState(false);
  const [destinationNetwork, setDestinationNetwork] = useState();

  const [destination, setDestination] = useState(
    chainId && chainId === 56 ? "0" : "1"
  );
  const { data: destinationBalance } =
    useDestinationTokenBalance(destinationNetwork);

  function confirmDialogToggle(e) {
    e.preventDefault();
    setShowConfirmDialog(!showConfirmDialog);
  }

  async function initiateTransfer(e) {
    try {
      e.preventDefault();
      const amount = transferAmount.toString();
      console.log({ amount, transferAmount, destination });
      setShowConfirmDialog(!showConfirmDialog);
      await depositToBridge(amount, destination);
    } catch (err) {
      console.log(err);
    }
  }

  const onDestinationChange = async (option) => {
    setDestination(option.value);
    setDestinationNetwork(option.chainId);
  };

  const currentNetwork = chainsSelect.find((x) => x.chainId === chainId);

  const isTransferButtonDisabled =
    transferAmount <= 0 ||
    !currentNetwork ||
    !destinationToName(destination) ||
    parseFloat(tokenBalance) < transferAmount;

  return (
    <StyledForm>
      <FormGroup>
        <StyledLabel>FROM</StyledLabel>
        <StyledFromNetwork>
          {currentNetwork ? (
            <CustomPlaceholder data={currentNetwork} />
          ) : (
            <StyledUnsupported>
              Not supported chain (id: {chainId})
            </StyledUnsupported>
          )}
        </StyledFromNetwork>
      </FormGroup>
      <FormGroup>
        <AmountInput
          setTransferAmount={setTransferAmount}
          transferAmount={transferAmount}
          tokenBalance={tokenBalance}
        />
      </FormGroup>
      <FormGroup>
        <StyledLabel>TO</StyledLabel>
        <Select
          onChange={onDestinationChange}
          options={filterAvailable(chainsSelect, chainId)}
          placeholder={"Choose a chain"}
          isSearchable={false}
          className='tonetwork'
          classNamePrefix='tonetwork'
          components={{
            Option: CustomSelectOption,
            SingleValue: CustomPlaceholder,
          }}
        />
        <StyledCurrentBalance>
          Current $FVT Balance: {destinationBalance || "--"}
        </StyledCurrentBalance>
      </FormGroup>
      {showConfirmDialog ? (
        <ConfirmDialog
          amount={transferAmount.toString()}
          from={currentNetwork.label}
          to={destinationToName(destination)}
          dialogSetter={confirmDialogToggle}
          transfer={initiateTransfer}
        />
      ) : null}
      <Button disabled={isTransferButtonDisabled} onClick={confirmDialogToggle}>
        DEPOSIT
      </Button>
    </StyledForm>
  );
};

export default Form;
