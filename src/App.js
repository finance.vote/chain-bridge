import Layout from "components/Layout";
import styled from "styled-components";
import "./App.css";
import Form from "components/Form";

const Container = styled.div`
  margin: 23px auto 0;
  padding: 37.1px 24px 31px 19px;
  box-shadow: 4px 6px 0 0 #00cfff;
  background-color: #0c2031;
  width: 34%;

  @media (max-width: 768px) {
    box-sizing: border-box;
    width: 100%;
  }
`;

function App() {
  return (
    <Layout>
      <Container>
        <Form />
      </Container>
    </Layout>
  );
}

export default App;
