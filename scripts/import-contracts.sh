#!/usr/bin/env bash

pwd
cp ../contracts/build/contracts/Token.json ./src/contracts/
cp ../contracts/build/contracts/Bridge.json ./src/contracts/
cp ../contracts/build/contracts/ERC20Handler.json ./src/contracts/
